# Iridium 9603 Interface
 
## Table of contents
 
* [Introduction](#introduction)
  * [Disclaimer](#disclaimer)
  * [Goals](#goals)
  * [About the Project](#about-the-project)
* [Task Breakdown](#task-breakdown)
  * [Phase I](#phase-i)
  * [Phase II](#phase-ii)
  * [Phase III](#phase-iii)
  * [Phase IV](#phase-iv)
* [Final Thoughts](#final-thoughts)
 
## Introduction
 
#### Disclaimer
This is an overview of the current progress on my Iridum 9603 SBD daughter board project. You are free to follow along, however this project has not been thoroughly tested and is still in development.  
 
I am not at fault for any damages incurred and all information has been provided as-is without warranty.
 
#### Goals
This project aims for the following:
* To enable hobbyist cheaper access to Iridium satellites
* To provide hobbyists an easy-to-understand guide on creating their own development boards
* To offer an alternative to expensive boards created by third party developers
* To have fun in learning more about satellite communications
 
#### About the Project
The project was conceptualized when I was looking into remote location and information reporting. The Iridium brand appeared to be the best option, as the antenna positioning seemed less finicky compared to other satellite technologies available. Addionally, there were many projects already existent with Raspberry Pi's and Arduinos that incorporated Iridium modems. These projects, however, used daughter boards that are grossly overpriced. For example:
 
| Board Type              | Price |
|-------------------------|-------|
| Brand X, daughter board | $250  |
| Brand Y, daughter board | $250  |
| Brand Z, breakout board | $145  |
| Brand Z, daughter board | $200  |
 
Having seen these prices and knowing I could accomplish the same at a fraction of the price, I decided to start this project in hopes of reducing the entry cost for accessing the Iridium platform.
 
## Task Breakdown
The tasks for is project have been broken down into four phases:
 
Phase I: Researching and developing breakout board solution<br>
Phase II: Testing and refining the breakout board<br>
Phase III: Researching and developing daughter board solution<br>
Phase IV: Testing and refining daughter board
 
### Phase I
Reasearch on how to create a breakout board for the Iridium 9603 was a breeze. The [Developer's Guide](/References/Iridium-9603-9603N_Developers-Guide.pdf) provided by Iridium was very straight forward and easy to follow. Granted, there isn't much magic to creating a breakout board.
 
As I did not have a circuit CAD software available off-hand, I decided to use KiCAD as it is free to use. The videos over at [Phil's Lab](https://www.youtube.com/user/menuuzer) made it easy to dive in and learn the software.
 
After going through some variations, I wound up with this design:
![KiCAD Mockup of the Iridium 9603 breakout board](/Images/P1_KiCAD_mockup.jpg)
 
I then sent these off to JLCPCB to be printed, as well as purchased the required 9603 [socket connector](https://www.digikey.com/en/products/detail/samtec-inc/SS4-10-3-00-L-D-K-TR/6561674) that was specified in the developer's guide. While waiting for the parts to arrive, I purchased a used 9603 off of eBay for about 50 USD, as well as set up a data plan with [Sat Phone Store](https://www.satphonestore.com) for about 20 USD for 12kb/mo
 
Once I received the parts, I assembled the breakout board and prepared for some basic functionality tests.
 
![Iridium 9603 board received from JLCPCB and with the connector reflowed](/Images/P1_breakout_result.png)
 
### Phase II

Here you can see the rather janky setup I had going for the Iridium 9603:<br>
![Setup used for testing the Iridium 9603 breakout board](/Images/P2_testing_setup.png)

As this is just a breakout board and not a daughter board, I am using a power supply that can supple the power as required in the documentation 5v, current limited, and with at least 1.5A current draw capable.

Here you can see the emails I sent and received to and from the 9603:<br>
![Emails received from and sent to the Iridium 9603](/Images/P2_data_send_receive.png)

And here you can see some of the serial terminal:<br>
![Data received via serial terminal](/Images/P2_data_received.png)

Some notes going forward:
 * Per the documentation, I would also add mounting holes. Although my current set up was secure enough, the mounting holes would add an extra layer of security and peace of mind.
 * Working with a sudo'd Arduino IDE, I lost the arduino code I was using to communicate with the 9603. Lesson learned. However I plan on moving to the STM32 platform, so this loss isn't much either way.
 
### Phase III

Phase III is still underway. During my research I stumbled upon these gits that may prove useful, as they show the thought process others have gone through while designing similar boards:

[Qwiic Iridium Project](https://github.com/sparkfunX/Qwiic_Iridium_9603N)

[Iridium Beacon Project](https://github.com/PaulZC/Iridium_9603_Beacon)

### Phase IV

TBD

## Final Thoughts

TBD
