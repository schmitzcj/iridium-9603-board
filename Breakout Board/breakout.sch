EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x10_Female Throughole1
U 1 1 6098B993
P 3600 3600
F 0 "Throughole1" H 3492 2875 50  0000 C CNN
F 1 "Conn_01x10_Female" H 3492 2966 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical_No_Outline" H 3600 3600 50  0001 C CNN
F 3 "~" H 3600 3600 50  0001 C CNN
	1    3600 3600
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x10_Female Throughole2
U 1 1 6098E168
P 7400 3500
F 0 "Throughole2" H 7428 3476 50  0000 L CNN
F 1 "Conn_01x10_Female" H 7428 3385 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical_No_Outline_No_First_Pin" H 7400 3500 50  0001 C CNN
F 3 "~" H 7400 3500 50  0001 C CNN
	1    7400 3500
	1    0    0    -1  
$EndComp
$Comp
L SS4-10-3.00-L-D-K-TR:SS4-10-3.00-L-D-K-TR SAMTECH1
U 1 1 60988A4A
P 5150 3100
F 0 "SAMTECH1" H 5600 3365 50  0000 C CNN
F 1 "SS4-10-3.00-L-D-K-TR" H 5600 3274 50  0000 C CNN
F 2 "SS4-10-3.00-L-D-K-TR:SS410350LDKTR" H 5900 3200 50  0001 L CNN
F 3 "http://suddendocs.samtec.com/prints/ss4-xx-x.xx-x-d-x-xx-tr-mkt.pdf" H 5900 3100 50  0001 L CNN
F 4 "Samtec SS4 Series 0.4mm 20 Way 2 Row Straight PCB Socket Surface Mount Board to Board" H 5900 3000 50  0001 L CNN "Description"
F 5 "4" H 5900 2900 50  0001 L CNN "Height"
F 6 "SAMTEC" H 5900 2800 50  0001 L CNN "Manufacturer_Name"
F 7 "SS4-10-3.00-L-D-K-TR" H 5900 2700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "200-SS4103.00LDKTR" H 5900 2600 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Samtec/SS4-10-300-L-D-K-TR?qs=jyzYFa4oMQh6w3qs7L%252B%2FJA%3D%3D" H 5900 2500 50  0001 L CNN "Mouser Price/Stock"
F 10 "SS4-10-3.00-L-D-K-TR" H 5900 2400 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/ss4-10-3.00-l-d-k-tr/samtec" H 5900 2300 50  0001 L CNN "Arrow Price/Stock"
	1    5150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4000 6050 4150
Wire Wire Line
	6050 4150 3900 4150
Wire Wire Line
	3900 4150 3900 4000
Wire Wire Line
	3900 4000 3800 4000
Wire Wire Line
	5150 4000 3950 4000
Wire Wire Line
	3950 4000 3950 3900
Wire Wire Line
	3950 3900 3800 3900
Wire Wire Line
	6050 3900 6100 3900
Wire Wire Line
	6100 3900 6100 4200
Wire Wire Line
	6100 4200 4000 4200
Wire Wire Line
	4000 4200 4000 3800
Wire Wire Line
	4000 3800 3800 3800
Wire Wire Line
	4050 3900 4050 3700
Wire Wire Line
	4050 3700 3800 3700
Wire Wire Line
	4050 3900 5150 3900
Wire Wire Line
	6050 3800 6150 3800
Wire Wire Line
	6150 3800 6150 4250
Wire Wire Line
	6150 4250 4100 4250
Wire Wire Line
	4100 4250 4100 3600
Wire Wire Line
	4100 3600 3800 3600
Wire Wire Line
	5150 3800 4150 3800
Wire Wire Line
	4150 3800 4150 3500
Wire Wire Line
	4150 3500 3800 3500
Wire Wire Line
	6050 3700 6200 3700
Wire Wire Line
	6200 3700 6200 4300
Wire Wire Line
	6200 4300 4200 4300
Wire Wire Line
	4200 4300 4200 3400
Wire Wire Line
	4200 3400 3800 3400
Wire Wire Line
	5150 3700 4250 3700
Wire Wire Line
	4250 3700 4250 3300
Wire Wire Line
	4250 3300 3800 3300
Wire Wire Line
	6050 3600 6250 3600
Wire Wire Line
	6250 3600 6250 4400
Wire Wire Line
	6250 4400 4300 4400
Wire Wire Line
	4300 4400 4300 3200
Wire Wire Line
	4300 3200 3800 3200
Wire Wire Line
	5150 3600 4350 3600
Wire Wire Line
	4350 3600 4350 3100
Wire Wire Line
	4350 3100 3800 3100
Wire Wire Line
	5150 3100 5050 3100
Wire Wire Line
	5050 3100 5050 2800
Wire Wire Line
	5050 2800 7100 2800
Wire Wire Line
	7100 2800 7100 4000
Wire Wire Line
	7100 4000 7200 4000
Wire Wire Line
	6050 3100 7050 3100
Wire Wire Line
	7050 3100 7050 3900
Wire Wire Line
	7050 3900 7200 3900
Wire Wire Line
	5150 3200 5000 3200
Wire Wire Line
	5000 3200 5000 2750
Wire Wire Line
	5000 2750 7000 2750
Wire Wire Line
	7000 2750 7000 3800
Wire Wire Line
	7000 3800 7200 3800
Wire Wire Line
	6050 3200 6950 3200
Wire Wire Line
	6950 3200 6950 3700
Wire Wire Line
	6950 3700 7200 3700
Wire Wire Line
	5150 3300 4950 3300
Wire Wire Line
	4950 3300 4950 2700
Wire Wire Line
	4950 2700 6900 2700
Wire Wire Line
	6900 2700 6900 3600
Wire Wire Line
	6900 3600 7200 3600
Wire Wire Line
	6050 3300 6850 3300
Wire Wire Line
	6850 3300 6850 3500
Wire Wire Line
	6850 3500 7200 3500
Wire Wire Line
	5150 3400 4900 3400
Wire Wire Line
	4900 3400 4900 2650
Wire Wire Line
	4900 2650 6800 2650
Wire Wire Line
	6800 2650 6800 3400
Wire Wire Line
	6800 3400 7200 3400
Wire Wire Line
	6050 3400 6750 3400
Wire Wire Line
	6750 3400 6750 3350
Wire Wire Line
	6750 3350 7200 3350
Wire Wire Line
	7200 3350 7200 3300
Wire Wire Line
	5150 3500 4850 3500
Wire Wire Line
	4850 3500 4850 2600
Wire Wire Line
	4850 2600 7150 2600
Wire Wire Line
	7150 2600 7150 3200
Wire Wire Line
	7150 3200 7200 3200
Wire Wire Line
	6050 3500 6700 3500
Wire Wire Line
	6700 3500 6700 3050
Wire Wire Line
	6700 3050 7200 3050
Wire Wire Line
	7200 3050 7200 3100
$EndSCHEMATC
